package data

import (
	"context"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/dom"
	"github.com/chromedp/cdproto/emulation"
	"github.com/chromedp/chromedp"
	"log"
	"strings"
	"sync"
	"time"
)

var target = "https://www.exploit-db.com"

type Parser struct {
	Log *log.Logger
}

// Parse asynchronous parser function, gets data out of chromedp, puts into dataSlice
func (p *Parser) Parse() ([]Data, error) {
	var dataSlice []Data
	htmlString, err := getHtmlData(target)
	if err != nil {
		p.Log.Println("Some error with ChromeDP: %v", err)
		return nil, err
	}

	dataChan := make(chan Data)

	var mainGroup sync.WaitGroup
	mainGroup.Add(1)


	go func(mainGroup *sync.WaitGroup, dataChan chan Data){
		defer mainGroup.Done()
		var wg sync.WaitGroup
		doc, _ := goquery.NewDocumentFromReader(strings.NewReader(htmlString))

		doc.Find("#exploits-table tbody tr").Each(func(i int, item *goquery.Selection) {
			wg.Add(1)
			go coParser(i, item, &wg, dataChan)
		})

		wg.Wait()
		close(dataChan)


	}(&mainGroup, dataChan)




	for data := range dataChan	{
		if nilCheck(&data) {
			dataSlice = append(dataSlice, data)
		}
	}

	mainGroup.Wait()




	return dataSlice, nil
}


func nilCheck(data *Data) bool{
	if data.Author != ""&&data.Code != ""&&data.Date!=""&&data.Title!=""&&data.Type!=""&&data.Platform!="" {
		return true
	}
	return false
}

// utility function for Parse()
func coParser(i int, item *goquery.Selection, wg *sync.WaitGroup, dataChan chan Data){
	defer wg.Done()
	var data Data

	item.Children().Each(func(index int, child *goquery.Selection) {

		if index == 0 {
			data.Date = child.Text()
		}
		if index == 4 {
			data.Title = child.Children().Text()
			codeLink, _ := child.Children().Attr("href")
			data.Code = target + codeLink
		}

		if index == 5 {
			data.Type = child.Children().Text()
		}

		if index == 6 {
			data.Platform = child.Children().Text()
		}

		if index == 7 {
			data.Author = child.Children().Text()
		}

	})

	dataChan<-data
}







func getHtmlData(url string) (string, error) {
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		chromedp.WithLogf(log.Printf),
	)
	defer cancel()
	ctx, cancel = context.WithTimeout(ctx, 15*time.Second)
	defer cancel()
	var htmlResult string

	err := chromedp.Run(ctx,
		emulation.SetUserAgentOverride("WebScraper 1.0"),
		chromedp.Navigate(url),
		chromedp.Sleep(1*time.Second),
		chromedp.Click(`verifiedCheck`, chromedp.ByID),
		chromedp.Sleep(1*time.Second),
		chromedp.WaitVisible(`tbody tr`, chromedp.ByQuery),
		chromedp.ActionFunc(func(ctx context.Context) error {
			node, err := dom.GetDocument().Do(ctx)
			if err != nil {
				return err
			}
			htmlResult, err = dom.GetOuterHTML().WithNodeID(node.NodeID).Do(ctx)
			return err
		}),
	)
	if err != nil {
		return "", err
	}

	return htmlResult, nil
}
