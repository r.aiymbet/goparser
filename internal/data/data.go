// Package data file for database operations
package data

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
)

type Data struct {
	Id       int    `json:"id"`
	Date     string `json:"date"`
	Title    string `json:"title"`
	Type     string `json:"type"`
	Platform string `json:"platform"`
	Author   string `json:"author"`
	Code     string `json:"code"`
}

type DataModel struct {
	DB  *pgxpool.Pool
	Log *log.Logger
}

func (d *DataModel) Save(data Data) (int, error) {
	stmt := `INSERT INTO parse_data (date, title, type, platform, author, code) VALUES($1, $2, $3, $4, $5, $6) returning id`

	var id int
	if err := d.DB.QueryRow(context.Background(), stmt, data.Date, data.Title, data.Type, data.Platform, data.Author, data.Code).Scan(&id); err != nil {
		return -1, nil
	}
	return id, nil
}

func (d *DataModel) FetchAll() ([]Data, error) {
	var dataSlice []Data
	stmt := `SELECT * FROM parse_data`
	rows, err := d.DB.Query(context.Background(), stmt)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var data Data
		err := rows.Scan(&data.Id, &data.Date, &data.Title, &data.Type, &data.Platform, &data.Author, &data.Code)
		if err != nil {
			return nil, err
		}
		dataSlice = append(dataSlice, data)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return dataSlice, nil

}

func (d *DataModel) SaveAll(dataSlice []Data) error {
	for i := 0; i < len(dataSlice); i++ {
		id, err := d.Save(dataSlice[i])
		if err != nil {
			d.Log.Printf("Some error occurred while inserting : %v\n", err)
			return err
		}
		d.Log.Printf("Successfully inserted with id : %d\n", id)
	}

	return nil
}
