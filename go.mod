module gitlab.com/r.aiymbet/goparser

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.0
	github.com/chromedp/cdproto v0.0.0-20210526005521-9e51b9051fd0
	github.com/chromedp/chromedp v0.7.3
	github.com/jackc/pgx/v4 v4.11.0
)
