package main

import (
	"context"
	"encoding/json"
	"flag"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/r.aiymbet/goparser/internal/data"
	"log"
	"os"
)

// general config storage
type config struct {
	port string
	addr string
}

func main() {
	var cfg config
	// retrieval of custom params
	flag.StringVar(&cfg.port, "port", ":4000", "Web API Port")
	flag.StringVar(&cfg.addr, "addr", "postgres://postgres:qwerty@localhost/parser_db?sslmode=disable", "DB Address")
	flag.Parse()

	log := log.New(os.Stdout, "Go Parser : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	conn, err := connectDB(cfg)

	if err != nil {
		log.Fatalf("error with DB connection: %v", err)
	}

	dataModel := data.DataModel{Log: log, DB: conn}

	dataSlice, err := dataModel.FetchAll()

	if err != nil {
		log.Fatalf("something went wrong: %v", err)
	}
	// convert data output to json
	json, err := json.Marshal(dataSlice)

	if err != nil {
		log.Fatalf("something went wrong: %v", err)
	}

	log.Println(string(json))
}
// database connection establishment
func connectDB(conf config) (*pgxpool.Pool, error) {
	pool, err := pgxpool.Connect(context.Background(), conf.addr)

	if err != nil {
		return nil, err
	}

	return pool, nil
}