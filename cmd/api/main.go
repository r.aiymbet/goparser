package main

import (
	"context"
	"flag"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/r.aiymbet/goparser/internal/data"
	"log"
	"os"
)

// general config is stored
type config struct {
	port string
	addr string
}

func main() {
	var cfg config

	// retrieval of custom params
	flag.StringVar(&cfg.port, "port", ":4000", "Web API Port")
	flag.StringVar(&cfg.addr, "addr", "postgres://postgres:qwerty@localhost/parser_db?sslmode=disable", "DB Address")
	flag.Parse()

	// logger outputs file, timestamp, name of distinct service
	log := log.New(os.Stdout, "Go Parser : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	// init of parser object
	parser := data.Parser{Log: log}
	conn, err := connectDB(cfg)

	if err != nil {
		log.Fatalf("error with DB connection: %v", err)
	}

	// init of dataModel object
	dataModel := data.DataModel{Log: log, DB: conn}

	dataSlice, err := parser.Parse()

	if err != nil {
		log.Fatalf("error with parsing: %v", err)
	}

	// retrieve data out of parser and save into database
	if err := dataModel.SaveAll(dataSlice); err != nil {
		log.Fatalf("error while insertion: %v", err)
	}

}

// func for db connection establishment
func connectDB(conf config) (*pgxpool.Pool, error) {
	pool, err := pgxpool.Connect(context.Background(), conf.addr)

	if err != nil {
		return nil, err
	}

	return pool, nil
}
